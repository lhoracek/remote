
package cz.lhoracek.remote;

public enum Buttons {
    LIVINGROOM_ON(R.id.button_1, "/livingroom/on"),
    LIVINGROOM_OFF(R.id.button_2, "/livingroom/off"),
    BEDROOM_ON(R.id.button_3, "/bedroom/on"),
    BEDROOM_OFF(R.id.button_4, "/bedroom/off"),
    ENTERTAINMENT_ON(R.id.button_5, "/entertainment/on"),
    ENTERTAINMENT_OFF(R.id.button_6, "/entertainment/off");

    private final int    buttonId;
    private final String restResource;

    private Buttons(int buttonId, String restResource) {
        this.buttonId = buttonId;
        this.restResource = restResource;
    }

    public int getButtonId() {
        return buttonId;
    }

    public String getRestResource() {
        return restResource;
    }

}
