/**
 *
 */

package cz.lhoracek.remote;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

/**
 * @author lhoracek
 */
public class ButtonBroadcastReciever extends BroadcastReceiver {

    /*
     * (non-Javadoc)
     * @see android.content.BroadcastReceiver#onReceive(android.content.Context, android.content.Intent)
     */
    @Override
    public void onReceive(Context context, Intent intent) {
        Log.d(ButtonBroadcastReciever.class.getName(), "Recieved broadcast");
        int ordinal = intent.getExtras().getInt(Buttons.class.getName());
        new RequestTask().execute(Buttons.values()[ordinal]);
    }

}
