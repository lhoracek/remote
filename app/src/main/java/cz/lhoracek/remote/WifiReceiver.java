package cz.lhoracek.remote;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.NetworkInfo;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.util.Log;

/**
 * Created by lhoracek on 9/21/15.
 */
public class WifiReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {

        NetworkInfo info = intent.getParcelableExtra(WifiManager.EXTRA_NETWORK_INFO);
        if (info != null) {
            //Intent serviceIntent = new Intent(context, OverlayService.class);
            //context.startService(serviceIntent);
            if (info.isConnected()) {
                // Do your work.

                // e.g. To check the Network Name or other info:
                WifiManager wifiManager = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
                WifiInfo wifiInfo = wifiManager.getConnectionInfo();
                String ssid = wifiInfo.getSSID();
                Log.d(getClass().getSimpleName(), "Network connected " + ssid);
            } else {
                Log.d(getClass().getSimpleName(), "Network disconected");
              //  context.stopService(serviceIntent);
            }
        }
    }
}