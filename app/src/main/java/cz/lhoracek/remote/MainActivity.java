
package cz.lhoracek.remote;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;

public class MainActivity extends FragmentActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction().add(R.id.container, new ButtonsFragment()).commit();
        }
    }

    /**
     * A placeholder fragment containing a simple view.
     */
    public static class ButtonsFragment extends Fragment implements OnClickListener {
        public ButtonsFragment() {}

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_main, container, false);
            for (Buttons button : Buttons.values()) {
                Button buttonView = (Button) rootView.findViewById(button.getButtonId());
                buttonView.setTag(button);
                buttonView.setOnClickListener(this);
            }
            return rootView;
        }

        @Override
        public void onClick(View v) {
            Intent intent = new Intent();
            intent.setAction("cz.lhoracek.remote.BROADCAST");
            intent.putExtra(Buttons.class.getName(), ((Buttons) v.getTag()).ordinal());
            getActivity().sendBroadcast(intent);
        }

    }

}
