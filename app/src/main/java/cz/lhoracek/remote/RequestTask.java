package cz.lhoracek.remote;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;

import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.HttpConnectionParams;

import android.os.AsyncTask;
import android.util.Log;

public class RequestTask extends AsyncTask<Buttons, Void, Void> {

    @Override
    protected Void doInBackground(Buttons... params) {
        HttpClient client = getHttpClient();
        HttpConnectionParams.setConnectionTimeout(client.getParams(), 1000);
        HttpConnectionParams.setSoTimeout(client.getParams(), 1000);
        Buttons button = params[0];
        String url = "http://192.168.1.11" + button.getRestResource();
        Log.i("Request", url);
        HttpGet request = new HttpGet();
        try {
            request.setURI(new URI(url));
        } catch (URISyntaxException e) {
            // nothing
        }

        try {
            client.execute(request);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    protected void onPostExecute(Void result) {
        super.onPostExecute(result);
    }

    protected HttpClient getHttpClient() {
        DefaultHttpClient client = new DefaultHttpClient();

        return client;
    }

}

